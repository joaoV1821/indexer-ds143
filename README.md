# Indexer Program

## Sinopse

Este programa, denominado "indexer", oferece funcionalidades de processamento e análise de documentos de texto, incluindo contagem de palavras, transformação para minúsculas, e opções para exibir estatísticas de frequência e busca por palavras-chave.

## Opções

- `--freq (n) (file)`: Exibe o número de ocorrências das n palavras mais frequentes no documento de parâmetro, ordenadas de forma decrescente.
- `--freq-word (word) (file)`: Contagem de uma palavra específica no documento de parâmetro.
- `--search (word) (file)`: Apresenta listagem de documentos relevantes para um dado termo de busca.

## Exemplos de Uso

```bash
python indexer.py --freq 10 example.txt
python indexer.py --freq-word welcome example.txt
python indexer.py --search programming document1.txt document2.txt document3.txt
