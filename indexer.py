# SINOPSE

# indexer --freq (n) (file)
# indexer --freq-word (word) (file)
# indexer --search (word) (file)
# tf(t,d) = count of t in d / number of words in d
# idf(t,D) = log_e [ n/count of documents with word t ]
# df(t) = occurrence of t in N documents
# idf(t) = N/df
# score(q,d) = sum {tf(t,d)*idf(t)} for all terms q in document d
# t — term (word) d — document (set of words) N — count of corpus corpus — the total document set
# O QUE DEVE FAZER
#   1.Contar palavras em documentos de texto
#   2.Transformar todas as palavras para minúsculas
#   3.Ignorar números e pontuações e palavras com menos de 2 caracteres
#       a. "Bem-vindo" -> "bem","vindo"

#   OPTIONS
# --freq: exibir número de ocorrências das n palavras mais frequentes no documento de parâmetro, ordem desc
# --freq-word: contagem de uma palavra específica no documento de parâmetro
# --search: apresenta listagem de documentos relevantes para um dado termo de busca

#   ESTRUTURA DE ARQUIVOS
#   1.Arquivos de código
#   2.README.md, com a explicação do processo de execução do programa
#       a. Professor clona o repositório, seguir as instruções do README e executar um programa chamado indexer

#   ARQUITETURA
#   1. Opções --freq e --freq-word: utilizarão uma tabela hash com encadeamento externo (uso eficiente da memória)
#       Valor de p será 31 (primo perto do número de letras do alfabeto e deve ser primo relativo a m)
#           MDC p,m = 1
#       Valor de m: será baseado no tamanho de arquivo e talvez algum tipo de proporção entre tamanho X n palavras
#           Próximo a potências de 2
#       Dimensionamento automático da tabela, buscando M ~ N/4 (m->posições, n->total de chaves)
#           Quando N/M >= 8, dobrar tamanho
#           Quando N/M <= 8, reduzir pela metade
#               Recálculo do hash
#

import sys
import os
import re
import time
from math import log10
from collections import Counter

class Calculate_TFIDF:
    def __init__(self, files, target_word):
        self.__files = files if isinstance(files, list) else [files]
        self.__target_word = target_word
        self.qtd_files = len(self.__files)

    def calculate_tf_for_word(self, file):
        count = 0

        with open(file, 'r', encoding='utf-8', errors='ignore') as f:
            text = ''.join(f.readlines()).lower()
            words = text.lower().split()
            counter = Counter(words)
            count = counter.get(self.__target_word.lower(), 0)

        tf_normalized = count / len(words) 

        return tf_normalized

    def calculate_tf_in_files(self):
        total_tf = 0

        try:
            for file in self.__files:
                total_tf += self.calculate_tf_for_word(file)

            if len(self.__files) > 0:
                average_tf = total_tf / len(self.__files)
                return average_tf
            else:
                return 0

        except FileNotFoundError:
            print(f"O arquivo '{file}' não foi encontrado.")
            return None
        except PermissionError:
            print(f"Sem permissão para acessar o arquivo '{file}'.")
            return None
        except Exception as e:
            print(f"Ocorreu um erro: {e}")
            return None
    def search_word(self):
        count = 0
        try:
            for file in self.__files:
                with open(file, 'r', encoding='utf-8', errors='ignore') as f:
                    content = f.read()
                    if self.__target_word.lower() in content.lower():
                        count += 1

            if len(self.__files) == 1:
                return count
            else:
                return count,  # Retorno no formato de tupla para consistência

        except FileNotFoundError:
            print(f"O arquivo '{file}' não foi encontrado.")
            return None
        except PermissionError:
            print(f"Sem permissão para acessar o arquivo '{file}'.")
            return None
        except Exception as e:
            print(f"Ocorreu um erro: {e}")
            return None
        
    def calculate_idf(self):
        search = self.search_word()
        if search > 0:
           
            return log10(len(self.__files) / search)
        else:
            return search

    def calculate_tfidf(self):
        return self.calculate_tf_in_files() * self.calculate_idf()


class Node:
    def __init__(self, key):
        self.key = key  # Palavra
        self.value = 1  # Contador
        self.next = None  # Next node : Encadeamento Externo


class HashTable:
    def __init__(self, file_size):
        capacity = self.hash_table_capacity(file_size)
        self.capacity = capacity
        self.size = 0
        self.upsize = 0
        self.downsize = 0
        self.table = [None] * capacity

    def insert(self, key: str):
        #index: int = self.string_hash(key, self.capacity)
        index: int = self.djb2_hash(key, self.capacity)

        if self.table[index] is None:
            self.table[index] = Node(key)
            self.size += 1
        else:
            current = self.table[index]
            while current:
                if current.key == key:
                    current.value += 1
                    return
                current = current.next
            new_node = Node(key)
            new_node.next = self.table[index]
            self.table[index] = new_node
            self.size += 1
        if self.size > 0.7 * self.capacity:
            if self.size/self.capacity > 8:
                self.resize_up()
            elif self.size/self.capacity < 4:
                self.resize_down()
    
    def search(self, key):
        #index: int = self.string_hash(key, self.capacity)
        index: int = self.djb2_hash(key, self.capacity)

        current = self.table[index]
        while current:
            if current.key == key:
                return current.value
            current = current.next
        print("Key not found on table. Returning...")
        raise KeyError(key)

    def __str__(self):
        elements = []
        for i in range(self.capacity):
            current = self.table[i]
            if current:
                elements.append(f"{i}-{(current.key, current.value)}")
                current = current.next
            while current:
                elements.append((current.key, current.value))
                current = current.next
        return str(elements)

    def __len__(self):
        print(f"size: {self.size}")
        print(f"capacity: {self.capacity}")
        print(f"n/m = {self.size/self.capacity}")
        print(f"resizes: {self.upsize} up / {self.downsize} down")
        return self.size

    def __contains__(self, key):
        try:
            self.search(key)
            return True
        except KeyError:
            return False

    def hash_table_capacity(self, file_size) -> int:
        capacity = ((file_size / 5) * 0.025) / 6
        capacity = self.primo_mais_perto(capacity)
        return int(capacity)

    def string_hash(self, key, capacity) -> int:
        p = 31
        hash_value = 0
        p_pow = 1

        for char in key:
            hash_value = (hash_value + (ord(char) - ord('a') + 1) * p_pow) % capacity
            p_pow = (p_pow * p) % capacity

        return hash_value

    def djb2_hash(self, key, capacity) -> int:
        hash_value = 5381
        for char in key:
            hash_value = ((hash_value << 5) + hash_value) + ord(char)
        return hash_value % capacity

    def resize_up(self):
        self.upsize += 1
        new_capacity = int(self.capacity * 2)
        new_table = [None] * new_capacity

        for i in range(self.capacity):
            current = self.table[i]
            while current:
                next_node = current.next
                #new_index = self.string_hash(current.key, new_capacity)
                new_index = self.djb2_hash(current.key, new_capacity)
                current.next = new_table[new_index]
                new_table[new_index] = current
                current = next_node

        self.capacity = new_capacity
        self.table = new_table

    def resize_down(self):
        self.downsize += 1
        if self.capacity > 1:
            new_capacity = int(self.capacity // 2)
            new_table = [None] * new_capacity

            for i in range(self.capacity):
                current = self.table[i]
                while current:
                    next_node = current.next
                    #new_index = self.string_hash(current.key, new_capacity)
                    new_index = self.djb2_hash(current.key, new_capacity)
                    current.next = new_table[new_index]
                    new_table[new_index] = current
                    current = next_node

            self.capacity = new_capacity
            self.table = new_table
            
    def is_prime(self, n):
        if n <= 1:
            return False
        for i in range(2, int(n**0.5) + 1):
            if n % i == 0:
                return False
        return True
    
    def primo_mais_perto(self, valor):
        primo_cima = None
        valor_cima = valor + 1
        while primo_cima is None:
            if self.is_prime(valor_cima):
                primo_cima = valor_cima
            valor_cima += 1

        return primo_cima


def file_word_counter(file) -> int:
    num_palavras = 0
    start_time = time.time()
    with open(file, 'r', encoding='latin-1', buffering= 10 * 1048576) as f:
        for line in f:
            palavras = line.split()
            num_palavras += len(palavras)
    #print(f'O arquivo {f.name} contém {num_palavras} palavras.')
    end_time = time.time()
    elapsed_time = end_time - start_time
    #print("Word counting time", elapsed_time)
    return num_palavras


def processing_line(line, table):
    word_pattern = r'\b[a-zA-Z]{3,}\b'
    words = re.findall(word_pattern, line)
    lowercased_words = [word.lower() for word in words]
    for word in lowercased_words:
        table.insert(word)
    return lowercased_words


def insertion_sort(arr, left=0, right=None):
    if right is None:
        right = len(arr) - 1

    for i in range(left + 1, right + 1):
        key_item = arr[i]
        j = i - 1

        while j >= left and arr[j][1] < key_item[1]:
            arr[j + 1] = arr[j]
            j -= 1

        arr[j + 1] = key_item


def merge(left, right):
    if not left:
        return right

    if not right:
        return left

    if left[0][1] > right[0][1]:
        return [left[0]] + merge(left[1:], right)

    return [right[0]] + merge(left, right[1:])


def timsort(arr):
    min_run = 32
    n = len(arr)

    for i in range(0, n, min_run):
        insertion_sort(arr, i, min((i + min_run - 1), n - 1))

    size = min_run
    while size < n:
        for start in range(0, n, size * 2):
            midpoint = min((start + size - 1), (n - 1))
            end = min((start + size * 2 - 1), (n - 1))

            merged_array = merge(
                left=arr[start:midpoint + 1],
                right=arr[midpoint + 1:end + 1]
            )

            arr[start:start + len(merged_array)] = merged_array

        size *= 2

    return arr


def find_top_n_words(hash_table, n: int):
    top_words = []

    start = time.time()
    for node in hash_table.table:
        current = node
        while current:
            if len(top_words) < n:
                top_words.append((current.key, current.value))
            else:
                if current.value > top_words[-1][1]:
                    top_words.pop()
                    top_words.append((current.key, current.value))
                    top_words = timsort(top_words)
            current = current.next
    end = time.time()
    #print(f"Sorting time {end - start}")
    return top_words


def freq(num, file):
    num = int(num)

    if not os.path.isfile(file):
        print("Not a valid file. Returning...")
        return

    if not file.endswith(".txt"):
        print("Invalid file extension: expected .txt. Returning...")
        return

    file_size = os.path.getsize(file)
    my_table = HashTable(file_size)
    #print(f"Initial capacity: {my_table.capacity}")
    start_time = time.time()

    with open(file, 'r', encoding='latin-1', buffering=10 * 1048576) as f:
        for line in f:
            processing_line(line, my_table)

    end_time = time.time()
    elapsed_time = end_time - start_time
    #print("Processing words time ", elapsed_time)
    #print(my_table.__str__())
    #print(my_table.__len__(), " palavras únicas")
    if num > my_table.size:
        print("N is greater than Table size, returning...")
    arr = find_top_n_words(my_table, num)

    print("")
    print("***********Top Words**************")
    i = 1
    for word in arr:
        print(f"{i}-{word}")
        i += 1


def freq_word(word, file):
    word = re.sub(r'\W+|\d+', '', word)
    if not os.path.isfile(file):
        print("Not a valid file, returning...")
        return

    if not file.endswith(".txt"):
        print("Invalid file extension, returning...")
        return

    file_size = os.path.getsize(file)
    my_table = HashTable(file_size)
    #print(f"Initial capacity: {my_table.capacity}")
    start_time = time.time()
    with open(file, 'r', encoding='latin-1', buffering=10 * 1048576) as f:
        for line in f:
            processing_line(line, my_table)

    end_time = time.time()
    elapsed_time = end_time - start_time
    #print("Processing words time ", elapsed_time)
    #print(my_table.__str__())
    #print(my_table.__len__(), " palavras únicas")
    frequency = my_table.search(word)
    print(f"A palavra *{word}* apareceu {frequency} vezes")


def proportion_numwords_numbytes(file):
    if not os.path.isfile(file):
        print("Not a valid file, returning...")
        return

    if not file.endswith(".txt"):
        print("Invalid file extension, returning...")
        return

    file_size = os.path.getsize(file)
    file_size_kb = file_size / 1024
    file_size_mb = file_size / (1024 * 1024)
    file_size_gb = file_size / (1024 * 1024 * 1024)
    print(f"{file_size} B, {file_size_kb} KB,{file_size_mb} MB, {file_size_gb} GB")

    num_words = file_word_counter(file)
    print(f"Cada palavra ocupa em média: {file_size / num_words :.2f} Bytes")

def get_tfidf(termo, docs):
    ranking = {}  # nome do arquivo e tfidf
    for doc in docs:
        print(f"Processando documento: {doc}")
        try:
            tfidf_calculator = Calculate_TFIDF([doc], termo)
            tfidf_value = tfidf_calculator.calculate_tfidf()
            ranking[doc] = tfidf_value
            print(f"TF-IDF calculado para {doc}: {tfidf_value}")
        except Exception as e:
            print(f"Erro ao calcular TF-IDF para {doc}: {e}")
    return ranking


def main():
    length = len(sys.argv)

    if length > 1:
        option = sys.argv[1]
        if option == "--freq-word":
            if length != 4:
                print("Wrong number of arguments, closing...")
                return 0
            freq_word(sys.argv[2], sys.argv[3])
            return 2
        elif option == "--freq":
            if length != 4:
                print("Wrong number of arguments, closing...")
                return 0
            freq(sys.argv[2], sys.argv[3])
            return 1
        elif option == "--proportion":
            if length != 4:
                print("Wrong number of arguments, closing...")
                return 0
            proportion_numwords_numbytes(sys.argv[2])
            return 999
        elif option == "--search":
            if length >= 5:
                term = sys.argv[2]
                docs = sys.argv[3:]
                word_pattern = r'\b[a-zA-Z]{3,}\b'
                
                if not re.findall(word_pattern, term):
                    print("O termo deve conter apenas palavras")
                
                if len(term) <= 2:
                    print("O termo deve conter mais de dois caracteres")
                
            
                else:
                    ranking = get_tfidf(term, docs)
                    sorted_ranking = sorted(ranking.items(), key=lambda x: x[1], reverse=True)
                    print("---Documentos ordenados por ordem de relevância--")
                    for document, tfidf_value in sorted_ranking:
                        print(f"|{document}: {tfidf_value}")
                return 8
        else:
            print("Option is not recognized, closing...")
            return 0
    else:
        print("No option detected, closing...")
        return 0


if __name__ == "__main__":
    sys.exit(main())
